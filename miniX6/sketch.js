let img;
let button = false;
let button1 = false;
let capture;
// let ctracker;
let names = ['John', 'Oscar', 'Anna', 'Ava', 'Elisabeth', 'Valentin', 'Eva'];
let ages = ['15', '19', '54', '75', '12', '25', '8', '63'];
let locations = ['Paris', 'New York', 'Bergen', 'Aarhus', 'Bruxelles'];
let emotions = ['Sad', 'Happy', 'Nervous', 'Excited', 'Afraid', 'Surprised'];
let words = ['We', 'Are', 'Still', 'Watching'];
let words1 = ['You', 'Cannot', 'Hide'];
let words2 = ['Thank', 'You', 'For', 'Clicking'];



function setup() {
  frameRate(6);
  createCanvas(1000,700);
  img = loadImage("image1.png");


   //button
  button = createButton('Accept');
  button.style('color','0');
  button.style("background","green");

  button.position(305,385);
  button.size(141,25);
  button.mousePressed(change);

  //button1
  button1 = createButton('Decline');
  button1.style('color','0');
  button1.style("background", 'white');

  button1.position(155,385);
  button1.size(141,25);
  button1.mousePressed(decline);

  //video
  capture = createCapture(VIDEO);
  capture.size();
  capture.hide();



}

function draw() {
  background(255);
  image(img, 0, 0, 600, 450);
  if (button == true) {
    image(capture, 650, 20, 250,200);


let name = random(names);
    fill(0);
    textSize(32);
    text('Name:', 660, 250);
    text(name, 620, 300);

  let age = random(ages);
  fill(0);
  textSize(32);
  text('Age:', 660, 350);
  text(age, 620, 400);

  let location = random(locations);
  fill(0);
  textSize(32);
  text('Location:', 660, 450);
  text(location, 620, 500);

  let emotion = random(emotions);
  fill(0);
  textSize(32);
  text('Emotion:', 660, 550);
  text(emotion, 620, 600);

  push();
  translate(mouseX,mouseY);
  fill(156, 240, 222);
  stroke(0);
  ellipse(0,0,30,30);
  ellipse(-15,-20,15,15);
  ellipse(5,-25,15,15);
  ellipse(20,-15,15,15);
  pop();

  }

  if (button1 == true) {

    image(capture, 650, 20, 250,200);

    let word = random(words);
    frameRate(3);
    fill(0);
    textSize(32);
    text(word,random(660), random(250));

    let word1 = random(words1);
    frameRate(3);
    fill(0);
    textSize(32);
    text(word1, random(750), random(300));

    let word2 = random(words2);
    frameRate(3);
    fill(0);
    textSize(32);
    text(word2, random(750), random(300));
  }

  // function faceTracker() {


  // }

}

function change() {
  button = true;




}

function decline() {
  button1 = true;
}
