Link to my program: https://sofielchristiansen.gitlab.io/aesthetic-programming/miniX6/
Link to my code: https://gitlab.com/sofielchristiansen/aesthetic-programming/-/blob/master/miniX6/sketch.js

# Aesthetic Programming - miniX6 

Which MiniX do you plan to rework?

I plan to rework my MiniX4. 

What have you changed and why?

I have changed my miniX4 in the way that I added a button next to the already existing button and called it “decline” with the intention to mimic the traditional accept or decline buttons that pop up when cookies are needed. Then if you click the new decline button the webcam turns on and appears on the screen. I also added words that randomly appear on the screen when the decline button is clicked which says for example "We are still watching", "Thank you for clicking" and "You cannot hide". I originally wanted to make the facetracker points appear instead of the webcam, but for some reason I couldn’t get it to work and the usual Friday shutup and code was not an option for getting help. 

How would you demonstrate aesthetic programming in your work?

Aesthetic programming is also not just about making a program or to code something just to code it, but it’s more about what you want to say with your program and the fact that it’s supposed to have a deeper meaning behind it. As the Aesthetic Programming book mentions in the preface chapter, the word ‘aesthetic’ doesn’t in this sense have anything to do with beauty as it’s commonly associated with, but rather it refers to the political aspect of aesthetics. This political view on aesthetics can be seen in my program in the sense that the main focus is not on the looks of the program, but it’s the consequences of the digital footprint we leave behind when for example clicking on certain buttons, images or links. 

What does it mean by programming as a practice, or even as a method for design?

I think programming and coding go very well with the practice of design in the way that they complement each other. Both parties are able to create something by themselves but combining the two gives the project a perspective from both sides of the technical as well as the design and conceptual thinking. Because this course is called aesthetic programming, it might not be relevant for this particular aspect, but one could also imagine that programming something without the human aspect that for example co-design offers, has a risk of ending in a result that the target group of users for example aren’t able to relate to.  


What is the relation between programming and digital culture?

The precise definition of digital culture is defined like this: “A digital culture is a concept that describes how technology and the internet are shaping the way that we interact as humans. It's the way that we behave, think and communicate within society.”
(https://gdsgroup.com/insights/technology/what-is-digital-culture/)
When using the online technology platforms we, as users, perform a certain behavior as well as when we for example are on the job, we use a different behavior and so on. Having learnt the basic elements of programming, I think helps to become more conscious about what is going on in the digital culture, and with this insight knowledge of for example how the collectable data is being used, are perhaps able to change our behavior online. This can in some way also be referred back to my program, or buttons online in general, in the way that the programming part of how a button looks, where it is placed, the size of it and so on can affect the user to do a certain behavior. 

Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?

My program demonstrates aesthetic programming in the sense that it doesn’t necessarily solve a problem, but it’s more likely to raise a discussion or a critical view on the topic of data capturing. My original program in my miniX4 also had an element of critical thinking in it as the intention of it was to raise awareness of all of the digital footprints and invisible data we are giving away when for example an acceptance button is clicked. I was then inspired by a thought that we discussed in the Software Studies class about how capturing of data and digital surveillance actually can’t be avoided if we want to make use of our technical devices and digital services which I think many people can’t imagine living without. In this sense it’s like we actively are providing data for the companies, businesses and perhaps private people to take and use however they want. On the other hand, you could argue that the reason for ‘giving away data’ is because we are getting used to use technology as a helping hand because it’s easier and almost everybody are willingly doing this. By adding the second button I wanted to go down the path of a wondering that lies in the question about the security of our data when a “decline” button is clicked for example. 

Lastly, I found this quote from the article concerning critical making: “The concept of critical making has many predecessors, all of which start with the assumption that built technological artifacts embody cultural values, and that technological development can be combined with cultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture.” (Ratto, Matt; Hertz, Garnet) As mentioned earlier there is a connection between programming and behavioral changes in digital culture, which this quote supports quite well.  

![](miniX6.png)
![](miniX6,1.png)


