# Aesthetic Programming - miniX2 

Link to my program: https://sofielchristiansen.gitlab.io/aesthetic-programming/miniX2/ 
Link to my code: https://gitlab.com/sofielchristiansen/aesthetic-programming/-/blob/master/miniX2/sketch.js  

Describe your program and what you have used and learned: 

In my second program I made two relatively simple emojis using the ellipse() shape as the face and eyes, the rect() shape as the first mouth with rounded edges and the arc() shape to make a half circle as the mouth on the second emoji. I have then written two different sentences of text by using the textStyle(ITALIC) syntaxes for both sentences. The first one says "fe_eling strees_sed?", in which two "_" (underscores) are incorporated into the sentences both because it creates an illusion that there are two smileys/emojis inside the words, but also to make it a little bit artistic. The background is placed in the function setup in order for the blue ellipse to follow the mouse and leave a trace behind the text and the emoji. Lastly there is placed a button underneath the emoji which answers the question with a "yes". If the button is clicked you then see another emoji, which has the same placement of the eyes and the mouth, but in stead of being stressed and kind of numb is now happy and relaxed. The text also changes to say "just bre_eathe". The point in this setup is for the user to take a deep breathe in as the emoji expands and breathe out as it shrinks. 

The syntaxes used for changing the two emojis is the if (mouseIsPressed){} syntax. Then i wrote the code for the second smiley and "just bre_eathe" between the two {} followed by the syntax } else { where I wrote the code for the first smiley and "fe_eling stres_sed?".  
When using the program you first notices the blue ellipse that's attached to the mouse and the point here is to move the mouse around in all the corners of the screen. This represents the stressful and anxious thoughts running through the mind when being stressed or when you have a lot on your mind. Then you read the question above the emoji, hold the mouse down on the "yes" button and a happy and relaxed emoji will expand and shrink in pace with your breathing.  
In this miniX I have learned how to write text, use different shapes to create an emoji, to use the syntax If (mouseIsPressed) {} and make it shift from something to another thing when the mouse is pressed. Lastly I have learned to use variables as well as an "If" statement to make an ellipse expand and shrink when reaching certain points of the canvas. 

How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?  

My overall focus of this program is to raise awareness of the importance of our mental health. I think in general and especially right now during a global pandemic, it's important to look after our mental health as much as our physical health. I was inspired by the app on my Apple watch, which from time to time reminds me just to breathe. I know from myself that in periods of being stressed, anxious or overwhelmed it's important to remind myself to sometimes stop and just take a deep breath in through the nose and a deep breath out through the mouth. Of course, it's not always enough to just breathe, but a lot of the time I personally think that taking a deep breathe is a good place to start. 
I used purple and pink/red to color the two emojis in order to just make them a little bit fun and colorful, but also to avoid the political problem with dividing people into colors. Our mental health is just as important to look after for a person with black, brown or asian skin as it is for a person with white skin. 
If this were to be a part of the emoji seletion on our phones the emoji representing the importance of our mental health might not look like the program I made, but I think it would be quite nice to have the option to remind oneself or a friend or a family member to look after our mental health and from time to time just to take a deep breath. 

![](miniX2-Runme.png)
![](miniX2Runme.png) 

