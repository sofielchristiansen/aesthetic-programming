Link to my program: https://sofielchristiansen.gitlab.io/aesthetic-programming/miniX4/
Link to my code: https://gitlab.com/sofielchristiansen/aesthetic-programming/-/blob/master/miniX4/sketch.js

# Aesthetic Programming - miniX4 

** Provide a title for and a short description of your work (1000 characters or less) as if you’re going to submit it to the festival: **


"Digital Hunting"

Inspired by the modern society and its development in data capturing and a surveillance culture this program, called Digital Hunting, shows the consequences of the digital behavior that is occurring today. The setup of the program is made with the purpose of looking like a standard website where the acceptance button is supposed to be clicked. When the user clicks the button the camera turns on and different kind of data is being captured - in other words, the user gives consent to much more than what it visible.       


** Describe your program and what you have used and learnt: **

The general message and focus on my program are to raise awareness around the digital footprint we leave behind when for example clicking on certain buttons, pictures, links and so on and the fact that we are being “watched” and the data we use is being saved somewhere in the world for someone to perhaps use in marketing or to resell it. 
I took a screenshot of a cooking website which asks the user to accept the cookies in order to see the website properly. I then uploaded the image to my program where I created a button in the same size, shape and color as the “accept” button on the cooking website and when the button is clicked the webcam of the computer will turn on and appear on the screen in the right top corner. Underneath the headlines “name”, “age”, “location”, and “emotions” will appear and below all headline there is an array of different names, ages, locations, and emotions. A mint-colored footprint is also added to follow the mouse’s X and Y coordinates which refers to the digital footprints that is being behind when for example clicking on a button, a picture, a headline, and so on. This represents a sampling of all the data that is collectable when for example a button is clicked. The emotion headline is added because after watching the documentary with Shoshana Zuboff where she points out that people, who analyze the given data is able to predict a user’s emotional state of mind. 
I have used different variables to create the button as well as turning on the webcam on the computer. Furthermore, I made arrays for the different names, ages, locations, and emotions below the headlines underneath the webcam. Within the arrays I used the random syntax in order to make it look like the computer is capturing different data depending on the user who clicks on the button. I also used an if statement in order to turn on the webcam and to show the text when clicking the button. To turn on the camera I made a createCapture in function setup and then an if statement in function draw to loop the capture if the button is clicked. Lastly I used the push(); and pop(); syntax to make a footprint that follows the mouseX and mouseY. 


** Articulate how your program and thinking address the theme of “capture all”. What are the cultural implications of data capture? **    

As mentioned earlier, the overall focus to my program is to raise awareness about the consequences of our actions online and the reality of how much unknown data we leave behind for others to take or use. After reading the concept and theme of “capture all” as well as the documentary about the modern surveillance capitalism I was surprised about the amount of data that we ourselves send out and in the end is used “against” ourselves in the sense to be manipulated to do something. My program doesn’t quite show all the different data that is collected, but it’s meant to give an idea of what is actually going on when for example an acceptance button is clicked. The two first lines of the “capture all” addresses this very well: 

“In a society ruled by algorithms, data is always at play. The drive towards the quantification of everything means that we are all contributing to a state of permanent capture of life into data.”

I think society as we know it today is due to the fact that everything is captured – all data is captured. It brings both advantages and disadvantages and I’m not sure how the world would look if this was drastically changed. Personally, I always have a feeling of being controlled and watched and it’s quite scary sometimes to think about how little influence we actually have in the digital world. But on the other hand, after some time I end up almost forgetting the manipulating and controlling data capturers and I continue to go down this path which probably is what these people wants us to do. Perhaps if enough awareness of the capturing of data would reach out to enough common people, questions and digital behavior changes would start to appear.   
 
![](miniX4_0.png)
![](miniX4_1.png)
