let img;
let button = false;
let capture;
let names = ['John', 'Oscar', 'Anna', 'Ava', 'Elisabeth', 'Valentin', 'Eva'];
let ages = ['15', '19', '54', '75', '12', '25', '8', '63'];
let locations = ['Paris', 'New York', 'Bergen', 'Aarhus', 'Bruxelles'];
let emotions = ['Sad', 'Happy', 'Nervous', 'Excited', 'Afraid', 'Surprised'];



function setup() {
  frameRate(6);
  createCanvas(1000,700);
  img = loadImage("image2.jpg");


   //button
  button = createButton('Accept');
  button.style('color','0');
  button.style("background","green");

  button.position(305,385);
  button.size(141,25);
  button.mousePressed(change);

  //video
  capture = createCapture(VIDEO);
  capture.size();
  capture.hide();



}

function draw() {
  background(255);
  image(img, 0, 0, 600, 450);
  if (button == true) {
    image(capture, 650, 20, 250,200);


let name = random(names);
    fill(0);
    textSize(32);
    text('Name:', 660, 250);
    text(name, 620, 300);

  let age = random(ages);
  fill(0);
  textSize(32);
  text('Age:', 660, 350);
  text(age, 620, 400);

  let location = random(locations);
  fill(0);
  textSize(32);
  text('Location:', 660, 450);
  text(location, 620, 500);

  let emotion = random(emotions);
  fill(0);
  textSize(32);
  text('Emotion:', 660, 550);
  text(emotion, 620, 600);

  push();
  translate(mouseX,mouseY);
  fill(156, 240, 222);
  stroke(0);
  ellipse(0,0,30,30);
  ellipse(-15,-20,15,15);
  ellipse(5,-25,15,15);
  ellipse(20,-15,15,15);
  pop();

  }

}

function change() {
  button = true;



}
