Link to my program: https://sofielchristiansen.gitlab.io/aesthetic-programming/miniX5/
Link to my code: https://gitlab.com/sofielchristiansen/aesthetic-programming/-/blob/master/miniX5/sketch.js

# Aesthetic Programming - miniX5 

What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior? 

Rule 1: 
-	If the bouncing ball is touching the width of the canvas, then random sizes of circles with random white, black and grey colors appear along with negative words that are written in the same colors and placed randomly on the canvas. 
Rule 2:
-	If the bouncing ball is touching the height of the canvas, then random sizes of circles with random red, blue and green colors appear along with positive words that are written in the same colors and placed randomly on the canvas. 

Over time the canvas will be filled completely with random sizes and colors of circles and after letting the program run for a while it almost looks like blobs of paint. The random words will of course continue as well. What each rule sort of represents, or what the program in general symbolizes, is the state of my brain in the sense that I often struggle with negative thoughts, the grey, white and black circles, but at the same time is working on bringing positive thoughts into my daily mindset. In result, when having run the program for long enough, the canvas is filled with half negative and half positive thoughts, essentially (possibly) creating a realistic image of most peoples’ minds. 
The rules in my program plays a very important part because it constitutes the entirety of making the program run as well as the deeper meaning and thinking that lies behind. 

Draw upon the assigned reading, how does this miniX help you understand the idea of “auto-generator” Do you have any further thoughts on the theme of this chapter? 

Having read the assigned readings, I have learnt a lot about “auto-generator” as well as generative art (mostly from software studies though). Having said that I think my program leans more towards the genre of generative art as it doesn’t solve any problems but has a more critical and reflecting part to it and the fact that no interaction is needed in order to make it run – it runs infinitively, and it gives random outcomes every time the program is refreshed. I have a better understanding for the use of rules and how they can control what happens in a program without any interference from the user. I also have a good understanding for how generative programs can be seen as an artwork - a program that is able to create an image or pattern, that perhaps or perhaps not were intended, but with certain rules are able to create something that the human interacter perhaps hadn't the imagination to in the first place. Like Langton's Ants, where the ant pattern only were discovered after having run the program for a while and was then given the name because it looked like ants running around.  

![](miniX5.png)

