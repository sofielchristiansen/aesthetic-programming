Link to my program: https://sofielchristiansen.gitlab.io/aesthetic-programming/miniX7/


# Aesthetic Programming - miniX7 

(I don't know why, but my link to my program doesn't seem to work, and since it's Easter I can't get hold on Winnie or any of the instructors. Just to clarify that I know it doesn't work, but I also don't know how to fix it) 

Describe how does/ do your game/ game object work? 

The key concept and structure of my game is based on the video from Daniel Shiffman where he creates a game with a unicorn that jumps above a moving train. My game consists of the same concept, but I decided to make it winter themed where a snowman is jumping over a moving sun in order to avoid melting. To make the snowman jump you press the spacebar down. I also added a slider underneath the program to make the speed slower or faster which is a possibility for the plater to adjust the difficulty of the game. 

Describe how you program the objects and their related attributes, and the methods in your game? 

I made two separate classes – one for the jumping snowman and one for the moving sun. I in a way added a certain behavior to both the snowman and the sun as in reality it’s not possible to have a jumping snowman and several moving suns, which also doesn’t represent the real size of the sun. The methods I used to make the objects have this behavior was the move() and show() function where I then mapped the speed of the sun to the slider. Inside each class I made a constructor in order to initialize the objects with a set of variables that has the purpose of indicating things like the position and size of the object and for the jumping snowman how much gravity there is when jumping.  

Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implication of abstraction?

In the setup portion of the Aesthetic Programming book, it mentions that computation is able to shape certain behaviors and actions among users. It further mentions how objects in object-oriented programming is about the interactions between and with the computational. 
When programming in an object-oriented programming language (OOP) it’s can be visualized like creating an object is like creating a person. In creating this object, it has to be assigned a property/several properties, a behavior and a method to execute this.  
Most programming languages and coding programs use object-oriented programming – in fact it’s hard to find a program that doesn’t use OOP. Instead of writing the code in the same file, the object-oriented programming technique is used to categorize different objects separately so that each object is able to have its own file with separate data, behaviors, properties, skills and so on. 
As the book mentions, abstraction is one of the key concepts in OOP where programs are organized around data, or objects and not function and logic.   

Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being abstracted? 

When thinking of the gaming culture today and trying to connect my program to a cultural context I thought about the general evolvement of playing video games. The gaming culture has definitely changed over the past decades as has peoples’ demands and needs in playing digital games. It’s common today to have high expectations when modern technology and programming abilities offers a variety of possibilities to program a good quality game. It’s possible to make games with for example 3D, virtual reality, high quality animations and so on. I then had the thought of questioning whether simple games, like my own miniX7, is still relevant to the modern gaming standards. A lot of people use games as an escape of their reality exactly because these high-quality games allow the users to create their own “fake reality”, whereas simple games with simple coding behind it doesn’t allow an escape option in the same way. The way the games are played has probably also changed. When games first made their appearance the purpose of playing was to beat a high score or compete with others, but today it’s in a lot of cases more about avoiding one’s own reality and wanting to live a different life in the game. Creating an alternate life in a game can in some ways be rewarding, but on the other side also dangerous with the risk of being almost “sucked” into the game, and therefore living in the game rather than in reality. The risk of something like that happening is much less likely in a simple game like my program because it’s much less realistic and most probably not entertaining enough for longer periods of time. 

The objects used in my program is still quite recognizable in the game, but it’s also clear how they differentiate from the real objects of the real world. For example, it’s possible to recognize that it’s a snowman that is jumping but comparing this object to the real world it’s obvious that this can’t happen. It’s the simple details, which gives a view of what the objects are supposed to look like and which behaviors and properties they possess in the game. 

![](miniX7.png)
