let unicorn;
let s3Img;
let s1Img;
let b2Img;
let sunImg;
let trains = [];
let slider;
let button = false;


function preload() {
  s3Img = loadImage('snowman3.jpg');
  sunImg = loadImage('sun.png');
  s1Img = loadImage('snowman1.png');
  b2Img = loadImage('background2.png');


}

function setup() {
  createCanvas(1000,550);
  unicorn = new Unicorn();

  slider = createSlider(0, 10, 5);


}

function keyPressed() {
  if (key == ' ') {
    unicorn.jump();
  }
}

function draw() {

  if (random(1) < 0.005) {
    trains.push(new Train());
  }

  background(b2Img);

  fill(0);
  textSize(32);
  text('speed:' + slider.value(), 10, 50);

  for (let t of trains) {
    t.move();
    t.show();
    if (unicorn.hits(t)) {
      console.log('game over');
      textSize(32);
      text('GAME OVER', 400, 300);

      noLoop();
    }


  }



  unicorn.show();
  unicorn.move();


}
