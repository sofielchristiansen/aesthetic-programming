let button;
var moonDot = (20,20);
var moonDots = (10,10);

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(5);
  button = createButton('reveal the joke')
  button.position(600,650);


}

function draw() {
  background(0,50);

  push();
  translate(mouseX -55,mouseY -50);
  fill(230, 203, 34);
  triangle(30, 75, 58, 20, 86, 75);
  pop();

  push();
  translate(mouseX +60,mouseY +60);
  rotate(PI);
  fill(230, 203, 34);
  triangle(30, 75, 58, 20, 86, 75);
  pop();


  // empire state building
  let num = 9;
  push();
  translate(width/2,height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  fill(255);
  noStroke();
  rect(0,0,90,40);
  rect(5,-10,80,30);
  rect(10,-20,70,30);
  rect(15,-160,60,180);
  rect(20,-170,50,40);
  rect(25,-180,40,40);
  rect(30,-185,30,5);
  rect(35,-190,20,5);
  triangle(40,-190,50,-190,45,-250);
  line(45,-280,45,-250);
  pop();

//moon
fill(215, 217, 210);
ellipse(100,100,150,150);

fill(153, 152, 150);
noStroke();
ellipse(100,70,moonDot);
ellipse(70,100,moonDot);
ellipse(120,150,moonDot);
ellipse(140,100,30,30);
ellipse(85,140,30,30);
ellipse(60,60,30,30);
ellipse(140,55,30,30);
ellipse(50,135,25,25);

fill(84, 83, 82);
noStroke();
ellipse(100,65,moonDots);
ellipse(70,105,moonDots);
ellipse(90,140,15,15);
ellipse(145,100,15,15);
ellipse(55,60,15,15);
ellipse(145,60,15,15);
ellipse(120,145,moonDots);
ellipse(50,130,moonDots);



if (createButton && mouseIsPressed){
  //text
  fill(255);
  textStyle(ITALIC);
  textSize(25);
  text('the city that never sleeps?',515,50);
}




  /*rect(600,500,90,40);
  rect(605,490,80,30);
  rect(610,480,70,30);
  rect(615,300,60,180);
  rect(620,280,50,40);
  rect(625,260,40,40);
  rect(630,255,30,5);
  rect(635,250,20,5);
  triangle(640,250,650,250,645,200);
  line(645,150,645,200);
  */
}
