let font;
let data;
let hatecomments;
let sound;

function preload(){
  font = loadFont('finishedsympathy.otf');
  data = loadJSON('sketch.json');
  soundFormats('mp3');
  sound = loadSound('positivecomments.mp3');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(0.1);
  background(255);

  sound.play();

  textAlign(CENTER);
  textFont(font);
  textSize(35);
  fill(0);
  text(getRandomLook(), width/3, height/6);
  fill('red');
  text(getRandomPersonality(),width/3*2,height/6*2);
  fill(0);
  text(getRandomRace(),width/2,height/6*3);
  fill('red');
  text(getRandomSexuality(),width/3,height/6*4);
  fill(0);
  text(getRandomContent(),width/3*2,height/6*5);
}

function getRandomLook() {
  return random(data.hatecomments[0].comment);
}

function getRandomPersonality() {
  return random(data.hatecomments[1].comment);
}

function getRandomRace() {
  return random(data.hatecomments[2].comment);
}

function getRandomSexuality() {
  return random(data.hatecomments[3].comment);
}

function getRandomContent() {
  return random(data.hatecomments[4].comment);
}

function draw() {
  background(255);

  textAlign(CENTER);
  textFont(font);
  textSize(35);
  fill(0);
  text(getRandomLook(), width/3, height/6);
  fill('red');
  text(getRandomPersonality(),width/3*2,height/6*2);
  fill(0);
  text(getRandomRace(),width/2,height/6*3);
  fill('red');
  text(getRandomSexuality(),width/3,height/6*4);
  fill(0);
  text(getRandomContent(),width/3*2,height/6*5);
}
