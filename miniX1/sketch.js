function setup() {
  // put setup code here
  createCanvas(640,480);
  print("hello world");
}

function draw() {
  // put drawing code here
  background('#fae');
  ellipseMode(RADIUS);
  fill(255);
  ellipse(210, 80, 30, 30); // outer white ellipses
  ellipseMode(CENTER);
  fill(100);
  ellipse(200, 80, 30, 30); // inner gray ellipse
  ellipseMode(RADIUS);
  fill(255);
  ellipse(290, 80, 30, 30); // outer white ellipses
  ellipseMode(CENTER);
  fill(100);
  ellipse(300, 80, 30, 30); // inner gray ellipse
// magenta ellipse
  let c = color('magenta');
  fill(c);
  noStroke();
  ellipse(150, 150, 55, 55);
  // yellow ellipse
  a = color(255, 204, 0);
  fill(a);
  ellipse(190, 190, 55, 55);
  // dark green ellipse
  e = color('hsb(160, 100%, 50%)');
  fill(e);
  ellipse(245, 210, 55, 55);
  // blue ellipse
  b = color(50, 55, 100);
  fill(b);
  ellipse(295, 185, 55, 55);
  // brigt green ellipse
  f = color('#0f0');
  fill(f);
  ellipse(336, 150, 55, 55);
  // nose
  ellipse(245, 135, 20, 20);
// blue legs
  d = color('hsl(160, 100%, 50%)');
  fill(d);
  stroke(50, 55, 100);
  triangle(110, 400, 255, 400, 249, 340);
  triangle(249, 340, 255, 400, 400, 395);
  // the body
  line(250, 350, 245, 240);
  // right arm
  line(250, 300, 300, 340);
  // left arm
  line(250, 300, 200, 340);

}
