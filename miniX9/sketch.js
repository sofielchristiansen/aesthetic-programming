let url = "https://www.googleapis.com/customsearch/v1?";
// register: https://developers.google.com/custom-search/json-api/v1/overview
let apikey = "AIzaSyDju9a-UV_W9-pGwnek41YrCYf7VuvhxF8";//"INPUT YOUR OWN KEY";
//get the searchengine ID: https://cse.google.com/all (make sure image is on)
let engineID = "012341178072093258148:awktesmhn9w"; //"INPUT YOUR OWN";
let query = "album+cover";  //search keywords
//check other parameters: https://tinyurl.com/googleapiCSE
let searchType = "image";
let imgSize ="medium";
let request; //full API

let getImg;
let getText;
let iteration;
let imgx;
let spacing;



function setup() {
	createCanvas(720, 720);
	background(0);
	frameRate(1);
	iteration = 0;
	imgx = 0;
	spacing = 150;
}



function draw() {

	if (frameCount%10 == 0)	{
		imgx += 150;
		iteration += 1;
		console.log(iteration);
		fetchImage();
	}

	if (getImg){
		loadImage(getImg, img=> {


				noStroke();
				fill(220);

				translate(imgx, spacing)
				image(img, 0, 0);
				imgLoaded = true;
				for (let col = 0; col < img.width; col += 5)	{
					for (let row = 0; row < img.height; row += 5)	{

						let c = img.get(col, row);
						stroke(color(c));
						strokeWeight(10);
						point(col,row);

					}
				}

			});
			if (imgx > 570)	{
				imgx = 150;
				spacing += 150;
			}
			if (iteration >= 8)	{
				noLoop();
			}
		}
		function fetchImage() {
			request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize +
			 "&q=" + query + "&searchType=" + searchType;
			console.log(request);
			loadJSON(request, gotData); //this is the key syntax to make API request
		}

		function gotData(data) {
			getImg = data.items[iteration].image.thumbnailLink;
			getText = data.items[iteration].title;
			console.log(getImg);
		}

		}
